<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{

    public function index()
    {
        $images = Image::paginate(9);
        return view('gallery.gallery')->with('images', $images);
    }
    public function store()
    {
        return view('gallery.add');
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'image' => 'required'
        ]);
        $images = $request->image;
        foreach ($images as $image) {
            $image_new_image = time() . $image->getClientOriginalName();
            $image->move('images', $image_new_image);
            $post = new Image;
            $post->user_id = Auth::user()->id;
            $post->image = 'images/' . $image_new_image;
            $post->save();
        }
        return redirect('/gallery');
    }

    public function destroy($id){
        $image = Image::find($id);
        $image->delete();
        return redirect('/gallery');

    }
}
