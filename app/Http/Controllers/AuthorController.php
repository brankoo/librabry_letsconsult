<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('authors.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ReThe POST method is not supported for this route. Supported methods: GET, HEAD.
    sponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:10',
            'surname' => 'required||string|max:10'
        ]);

        $author = new Author;
        $author->name = request('name');
        $author->surname = request('surname');

        $author->save();

        return redirect('/authors');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::findOrFail($id);
        return view('authors.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = "")
    {
        $data = $request->validate([
            'name' => 'required|string|max:10',
            'surname' => 'required||string|max:10'
        ]);

        Author::whereId($id)->update($data);

        return redirect('/authors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);
        $author->delete();
        return redirect('/authors');

    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        return array('id' => $array['id'],'name' => $array['name'], 'surname' => $array['surname']);
    }
}
