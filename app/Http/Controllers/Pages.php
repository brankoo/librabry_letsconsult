<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;


class Pages extends Controller
{
    public function home() {
        return view('welcome');
    }

    public function authors() {
        return view('authors.authors', [
            'authors' => Author::all(),
        ]);
    }

    public function contact()
    {
        return view('/contact');
    }

    public function about()
    {
        return view('/about');
    }

    public function club()
    {
        return view('/club');
    }

    public function book_show($id){
        Book::findOrFail($id);

        return view('news_show')->with('clanok',$article)->with('comments',$comments);
    }
}
