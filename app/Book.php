<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title', 'author', 'is_borrowed'
    ];

    public static function showPreview($str, $limit=120, $strip = false) {
        $str = ($strip == true)?strip_tags($str):$str;
        if (strlen ($str) > $limit) {
            $str = substr ($str, 0, $limit - 3);
            return (strip_tags(substr ($str, 0, strrpos ($str, ' ')).'...'));
        }
        return (strip_tags(trim($str)));

    }
}
