<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Pages@home');
Route::get('/books', 'Pages@books');
Route::resource('books', 'BookController');

//add
Route::get('/addbook', 'BookController@create');
Route::get('/addimage', 'ImageController@store');
Route::post('/image', 'ImageController@post');
Route::post('submit', 'BookController@store');

//delete
Route::get('delete-book', 'BookController@index');
Route::get('delete-book/{id}', 'BookController@destroy');
Route::delete('/image/{id}', 'ImageController@destroy');


//update
Route::get('edit-book/{id}', 'BookController@edit');
Route::post('update-book/{id}', 'BookController@update')->name('update-book');
Route::get('update-status', 'BookController@updateStatus')->name('update-status');

//gallery
Route::get('/gallery', 'ImageController@index' );

//contact
Route::get('/contact', 'Pages@contact' );

//about
Route::get('/about', 'Pages@about' );

//club
Route::get('/club', 'Pages@club' );

Route::group(['middleware' => ['web']], function () {
    Route::get('setlocale/{locale}', function (\Illuminate\Http\Request $requestApp, $locale) {
        if (in_array($locale, \Config::get('app.locales'))) {
            $requestApp->session()->put('locale', $locale);
        }
        return redirect()->back();
    });
});

Auth::routes([
//    'register' => false, // Registration Routes...
//    'reset' => false, // Password Reset Routes...
//    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', 'HomeController@index')->name('home');
