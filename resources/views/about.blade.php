
@extends('layouts.base')

@section('content')
    <div class="container">
        <div class="row" style="background-color:  rgba(245, 245, 245, 0.9);">
            <div class="col-lg-12">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="display-3 text-center">Kto sme?</h2><br>
                            <p class="lead text-center">Na začiatku bol les. A potom my mamy, ktoré čas strávený v prírode berú ako ten najprirodzenejší spôsob ako podporiť pohyb a chuť objavovať a spoznávať. A naše deti a ich nekonečná radosť a sústredenie pri preliezaní stromov, pozorovaní chrobákov a rýpaní sa konárom v zemi. V túžbe dopriať tento priateľský kontakt s prírodou nielen našim potomkom sme založili lesný klub Bystruška spadajúci pod občianske združenie nomad.um. Veríme totiž, že les je nielen najlepším ihriskom, ale i učiteľom a deti vyrastajúce v spojení s prírodou sa cítia šťastne a slobodne. A práve týmto našim lesným svetom ich chceme s láskou a rešpektom sprevádzať.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


