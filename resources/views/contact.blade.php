
@extends('layouts.base')

@section('content')
    <div class="banner">

    </div>
    <div class="container">
        <div class="row" style="background-color:  rgba(245, 245, 245, 0.9);">
            <div class="col-lg-12">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="display-3 text-center">@lang('words.kontakt')</h2>
                            <p class="lead text-center">@lang('words.kontaktPopis')</p>
                            <p class="lead text-center"><i class="fa fa-envelope-o" aria-hidden="true"></i> Mail : info@lesnyklubbystruska.sk</p>
                            <p class="lead text-center"><i class="fa fa-phone" aria-hidden="true"></i> Telefón : +421 9XX XXX XXX</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


