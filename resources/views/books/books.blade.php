@extends('layouts.base')

@section('content')

<main role="main">
    <div class="album bg-light">
        <div class="row">
                @foreach($books as $book)
                    <div class="col-md-12">
                        <div class="card flex-md-row mb-4 box-shadow h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                                <a href="/books/{{ $book->id }}">
                                    <h3 style="color: black" class="card-text">{!! $book->title !!}</h3>
                                </a>
                                <div class="mb-1 text-muted">{{ $book->updated_at->diffForHumans() }}</div>
                                <p>{!! \App\Book::showPreview($book->text)!!}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    @auth
                                        <div class="btn-group">
                                            <a href = '/delete-book/{{ $book->id }}' class="btn btn-sm btn-outline-dark bg-danger">Delete</a>
                                            <a href="/edit-book/{{ $book->id }}" class="btn btn-sm btn-outline-dark bg-warning">Edit</a>
                                        </div>
                                    @endauth
                                </div>
                            </div>
                            <img src="{{URL::asset('img/thumbnail.jpg')}}" style="width: 200px; height: 250px;" class="img-fluid card-img-right flex-auto d-none d-md-block">
                        </div></div>
                @endforeach
            </div>
        </div>
</main>
@endsection
