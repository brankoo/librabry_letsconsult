
@extends('layouts.base')

@if ($errors->any())

    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

        @endforeach
    </ul>

@endif

@section('content')
    <div class="col" style="background-color:  rgba(245, 245, 245, 0.9);">
        <h1>@lang('words.upravitClanok')</h1><br>
        <form action="{{ route('update-book', $book->id) }}" method="POST" class="book-form">
            @csrf
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">@lang('words.nadpis')</label>
                <div class="col-sm-10">
                    <input value="{{old('title', $book->title)}}" class="form-control col-sm" name="title">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">@lang('words.text')</label>
                <div class="col-sm-10">
                    <textarea class="ckeditor form-control col-sm" name="text">{!! old('title', $book->text)!!}</textarea>
                </div>
                </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">@lang('words.potvrdit')</button>
                </div>
            </div>
        </form>
    </div>
@endsection
