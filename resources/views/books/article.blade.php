@extends('layouts.base')

@section('content')

<article class="book-article">
    <header>
        <h2 class="title">
                {{ $book->title }}
        </h2>
    </header>

    <div class="content">
        <p>{!! nl2br($book->text) !!}</p>
    </div>
</article>
@endsection
