
@extends('layouts.base')

@if ($errors->any())

    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

        @endforeach
    </ul>

@endif

@section('content')
    <div class="col" style="background-color:  rgba(245, 245, 245, 0.9);">
        <h1>@lang('words.pridatNovuFotku')</h1><br>
        <form action="/image" enctype="multipart/form-data" method="POST" class="book-form">
            @csrf
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">@lang('words.vybratFotku')</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control image" name="image[]" multiple accept="image/*">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <input type="submit" value="@lang('words.pridat')" class="btn btn-success">
                </div>
            </div>
        </form>
    </div>
@endsection
