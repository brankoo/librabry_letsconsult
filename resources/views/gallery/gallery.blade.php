
@extends('layouts.base')

@section('content')

<div class="container">
    <div class="row">
    @forelse($images as $image)
    <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12" style="padding-bottom: 15px;">
        <div class="card">
            <a href="#" class="pop"><img id="myImg" src="{{ asset($image->image) }}" alt="Broken" class="card-img-top rounded" style=" max-height: 200px; width: 100%" ></a>
            @auth
                <div class="card-body">
                    <form action="/image/ {{ $image->id }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                </div>
            @endauth
        </div>
    </div>
    @empty
    <h1 class="text-danger">@lang('words.niesuPridaneZiadneObrazky')</h1>
    @endforelse
    </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <img src="" class="imagepreview" style="width: 100%; ">
        </div>
    </div>
</div>

@endsection
