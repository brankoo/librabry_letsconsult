
@extends('layouts.base')

@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center" style="background-color:  rgba(245, 245, 245, 0.9);">
        <h1 class="display-4">Lesný klub Bystruška</h1>
        <h1>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bug" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.355.522a.5.5 0 0 1 .623.333l.291.956A4.979 4.979 0 0 1 8 1c1.007 0 1.946.298 2.731.811l.29-.956a.5.5 0 1 1 .957.29l-.41 1.352A4.985 4.985 0 0 1 13 6h.5a.5.5 0 0 0 .5-.5V5a.5.5 0 0 1 1 0v.5A1.5 1.5 0 0 1 13.5 7H13v1h1.5a.5.5 0 0 1 0 1H13v1h.5a1.5 1.5 0 0 1 1.5 1.5v.5a.5.5 0 1 1-1 0v-.5a.5.5 0 0 0-.5-.5H13a5 5 0 0 1-10 0h-.5a.5.5 0 0 0-.5.5v.5a.5.5 0 1 1-1 0v-.5A1.5 1.5 0 0 1 2.5 10H3V9H1.5a.5.5 0 0 1 0-1H3V7h-.5A1.5 1.5 0 0 1 1 5.5V5a.5.5 0 0 1 1 0v.5a.5.5 0 0 0 .5.5H3c0-1.364.547-2.601 1.432-3.503l-.41-1.352a.5.5 0 0 1 .333-.623zM4 7v4a4 4 0 0 0 3.5 3.97V7H4zm4.5 0v7.97A4 4 0 0 0 12 11V7H8.5zM12 6H4a3.99 3.99 0 0 1 1.333-2.982A3.983 3.983 0 0 1 8 2c1.025 0 1.959.385 2.666 1.018A3.989 3.989 0 0 1 12 6z"/>
            </svg>
        </h1>
        <p class="lead">
            Určený pre deti vo veku od 2,5 do 6 rokov.
        </p>
    </div>
    <div class="container" style="background-color:  rgba(245, 245, 245, 0.9);">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Prečo?</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Sme presvedčení o tom, že najlepším priestorom pre prirodzený rozvoj detí je les a príroda. </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Kedy?</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Pon: <strong>9:00 - 12:00</strong></li>
                            <li>Str: <strong>9:00 - 12:00</strong></li>
                            <li>Pia: <strong>9:00 - 12:00</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">S kým?</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Počet detí v skupine je max. 12</li>
                            <li><strong>+</strong></li>
                            <li>2 sprievodcovia.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Kde?</h4>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title pricing-card-title">Vždy po dohode:</h5>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Lesy Devínskej Kobyly</li>
                            <li>Sandberg a lesostep</li>
                            <li>Okolie rieky Moravy</li>
                            <div class="dropdown-divider"></div>
                            <li>V prípade nepriaznivého počasia využívame priestory F-centra v Devínskej Novej Vsi.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Cena?</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">€120</h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>/ mesiac</li>
                            <li>/ dieťa</li>
                            <li>Zahrnutá desiata</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-xs-12 mb-4 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Cena?</h4>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title pricing-card-title">Denný program:</h5>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li><strong>8:50 - 9:00 </strong>príchod a privítanie</li>
                            <li><strong>9:00 - 9:30 </strong>ranný kruh</li>
                            <li><strong>9:30 - 10:00 </strong>desiata</li>
                            <li><strong>10:00 - 11:45 </strong>voľná hra / aktivity v prírode</li>
                            <li><strong>11:45 - 12:00 </strong>rozlúčka</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <p class="lead text-center">
            <a href="/contact" class="btn text-light btn-lg bg-grey">Chcem sa dozvedieť viac</a>
        </p>
    </div>
@endsection
