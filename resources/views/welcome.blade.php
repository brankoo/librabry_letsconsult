<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="footer, address, phone, icons" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="public/css/footer.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

    <style>
        body {
            font-family: Calibri;
        }

        main {
            background-color: #f7f7f7;
        }

        .bg-grey {
            background-color: #7a7a7a;
        }
        :root {
            --jumbotron-padding-y: 3rem;
        }

        .jumbotron p:last-child {
            margin-bottom: 0;
        }

        .jumbotron .container {
            max-width: 40rem;
        }

        footer {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        footer p {
            margin-bottom: .25rem;
        }

        footer{
            bottom: 0;

        }
        .footer-distributed{
            box-shadow: #f8f9fa;
            box-sizing: border-box;
            width: 100%;
            text-align: left;
            font: bold 16px sans-serif;
            padding: 55px 50px;
        }

        .footer-distributed .footer-left,
        .footer-distributed .footer-center,
        .footer-distributed .footer-right{
            display: inline-block;
            vertical-align: top;
        }

        .footer-distributed .footer-left{
            width: 40%;
        }

        .footer-distributed h3{
            color:  #ffffff;
            font: normal 36px 'Cookie', cursive;
            margin: 0;
        }

        .footer-distributed h3 span{
            color:  #f8f9fa;
        }

        .footer-distributed .footer-links{
            color:  #ffffff;
            margin: 20px 0 12px;
            padding: 0;
        }

        .footer-distributed .footer-links a{
            display:inline-block;
            line-height: 1.8;
            text-decoration: none;
            color:  inherit;
        }

        .footer-distributed .footer-company-name{
            color:  #f8f9fa;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
        }

        .footer-distributed .footer-center{
            width: 35%;
        }

        .footer-distributed .footer-center i{
            background-color:  #7a7a7a;
            color: #ffffff;
            font-size: 25px;
            width: 38px;
            height: 38px;
            border-radius: 50%;
            text-align: center;
            line-height: 42px;
            margin: 10px 15px;
            vertical-align: middle;
        }

        .footer-distributed .footer-center i.fa-envelope{
            font-size: 17px;
            line-height: 38px;
        }

        .footer-distributed .footer-center p{
            display: inline-block;
            color: #ffffff;
            vertical-align: middle;
            margin:0;
        }

        .footer-distributed .footer-center p span{
            display:block;
            font-weight: normal;
            font-size:14px;
            line-height:2;
        }

        .footer-distributed .footer-center p a{
            color:  #f8f9fa;
            text-decoration: none;;
        }

        .footer-distributed .footer-right{
            width: 20%;
        }

        .footer-distributed .footer-company-about{
            line-height: 20px;
            color:  #f8f9fa;
            font-size: 13px;
            font-weight: normal;
            margin: 0;
        }

        .footer-distributed .footer-company-about span{
            display: block;
            color:  #ffffff;
            font-size: 14px;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .footer-distributed .footer-icons{
            margin-top: 25px;
        }

        .footer-distributed .footer-icons a{
            display: inline-block;
            width: 35px;
            height: 35px;
            cursor: pointer;
            background-color:  #7a7a7a;
            border-radius: 2px;

            font-size: 20px;
            color: #ffffff;
            text-align: center;
            line-height: 35px;

            margin-right: 3px;
            margin-bottom: 5px;
        }


        @media (max-width: 880px) {

            .footer-distributed{
                font: bold 14px sans-serif;

            }

            .footer-distributed .footer-left,
            .footer-distributed .footer-center,
            .footer-distributed .footer-right{
                display: block;
                width: 100%;
                margin-bottom: 40px;
                text-align: center;
            }

            .footer-distributed .footer-center i{
                margin-left: 0;
            }

        }

        #banner {
            height: 350px;
            width: 200px;
            background: #7a7a7a;
            display: block;
            color: white;
            top: 500px;
            left: 0;
            border-style: solid;
            border-color: black;
        }

        .banner-button {
            float: right;
        }


        .hover_bkgr_fricc{
            background:rgba(0,0,0,.4);
            cursor:pointer;
            display:none;
            height:100%;
            position:fixed;
            text-align:center;
            top:0;
            width:100%;
            z-index:10000;
        }

        .hover_bkgr_fricc .helper{
            display:inline-block;
            height:100%;
            vertical-align:middle;
        }

        .hover_bkgr_fricc > div {
            background-color: #fff;
            box-shadow: 10px 10px 60px #555;
            display: inline-block;
            height: auto;
            max-width: 551px;
            min-height: 100px;
            vertical-align: middle;
            width: 60%;
            position: relative;
            border-radius: 8px;
            padding: 15px 5%;
        }

        .popupCloseButton {
            background-color: #fff;
            border: 3px solid #999;
            border-radius: 50px;
            cursor: pointer;
            display: inline-block;
            font-family: arial;
            font-weight: bold;
            position: absolute;
            top: -20px;
            right: -20px;
            font-size: 25px;
            line-height: 30px;
            width: 30px;
            height: 30px;
            text-align: center;
        }

        .popupCloseButton:hover {
            background-color: #ccc;
        }


    </style>
</head>

@include('layouts.navbar')

<body>
{{--alert banner--}}
<div id="banner" class="position-fixed rounded">
    <button class="banner-button" onclick="closeBanner()">X</button>
    <div class="banner-inner">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aspernatur beatae, cupiditate dolorem dolorum et excepturi fugit hic laborum mollitia nam sequi similique ut. Atque eum ipsam natus obcaecati reiciendis.</div>
</div>

<div class="hover_bkgr_fricc" id="priroda" style="display:none;">
    <span class="helper"></span>
    <div>
        <div onclick="document.getElementById('priroda').style.display='none';" class="popupCloseButton">&times;</div>
        <ul class="priroda">
            <li>objavovanie jedinečného prostredia ponúkajúceho nekonečné množstvo podnetov všetkými zmyslami</li>
            <li>budovanie pozitívneho vzťahu k prírode a jej obyvateľom v ich prirodzenom prostredí</li>
            <li>ekológia a ochrana prírody ako súčasť každodenného života</li>
            <li>aktívne a pravidelné spoznávanie prírody v jednotlivých ročných obdobiach a za každého počasia</li>
        </ul>
    </div>
</div>

<div class="hover_bkgr_fricc" id="priatelia" style="display:none;">
    <span class="helper"></span>
    <div>
        <div onclick="document.getElementById('priatelia').style.display='none';" class="popupCloseButton">&times;</div>
        <ul class="priroda">
            <li>príprava na pobyt v lesnej škôlke </li>
            <li>možnosť adaptácie v prítomnosti rodiča </li>
            <li>rozvíjanie sociálnych a komunikačných zručností prostredníctvom voľnej hry, priamej skúsenosti a spolupráce</li>
            <li>vekovo zmiešaný kolektív ako výhoda (starší sú vzorom pre mladších, ktorí zase učia starších zodpovednosti)</li>
        </ul>
    </div>
</div>

<div class="hover_bkgr_fricc" id="podpora" style="display:none;">
    <span class="helper"></span>
    <div>
        <div onclick="document.getElementById('podpora').style.display='none';" class="popupCloseButton">&times;</div>
        <ul class="priroda">
            <li>fantázie a estetického cítenia prostredníctvom kontaktu s umením</li>
            <li>kreativity a tvorivosti</li>
            <li>komunikačných schopností pod logopedickým vedením</li>
            <li>anglického jazyka hravou/zábavnou formou</li>
        </ul>
    </div>
</div>

<div class="hover_bkgr_fricc" id="pristup" style="display:none;">
    <span class="helper"></span>
    <div>
        <div onclick="document.getElementById('pristup').style.display='none';" class="popupCloseButton">&times;</div>
        <ul class="priroda">
            <li>lesný klub inšpirovaný waldorfskou pedagogikou a prvkami Montessori</li>
            <li>rešpektujúci k individuálnym potrebám dieťaťa a vedúci k samostatnosti</li>
            <li>podporujúci jedinečnosť každého dieťaťa</li>
            <li>bezpečný priestor a primerané hranice pre každého</li>
        </ul>
    </div>
</div>

<div class="hover_bkgr_fricc" id="pohyb" style="display:none;">
    <span class="helper"></span>
    <div>
        <div onclick="document.getElementById('pohyb').style.display='none';" class="popupCloseButton">&times;</div>
        <ul class="priroda">
            <li>les ako najlepšie ihrisko</li>
            <li>rozvíjanie hrubej a jemnej motoriky na podporu koordinácie a fyzickej zdatnosti</li>
            <li>podpora prirodzenej potreby pohybu</li>
            <li>pozitívny vplyv na telesné a duševné zdravie a posilňovanie imunitného systému</li>
        </ul>
    </div>
</div>

<main role="main">
    <div class="album py-5">
        <div class="container">

            <div style="width: 100%" class="text-center cover-container d-flex h-80 p-3 mx-auto flex-column">

                <img id="img_ID" src="{{URL::asset('img/kruh.png')}}" alt="Circle" usemap="#map" border="0" width="100%" class="img-fluid">

                <map id="map_ID" name="map">
                    <area alt="Priroda" title="Priroda" onclick="document.getElementById('priroda').style.display='block';" coords="381,34,701,260" shape="rect">
                    <area alt="Priatelia" title="Priatelia" onclick="document.getElementById('priatelia').style.display='block';" coords="1029,531,738,317" shape="rect">
                    <area alt="Podpora" title="Podpora" onclick="document.getElementById('podpora').style.display='block';" coords="634,739,990,907" shape="rect">
                    <area alt="Pristup" title="Pristup" onclick="document.getElementById('pristup').style.display='block';" coords="182,697,399,971" shape="rect">
                    <area alt="Pohyb" title="Pohyb" onclick="document.getElementById('pohyb').style.display='block';" coords="56,266,325,507" shape="rect">
                </map>

            </div>

        </div>
    </div>
</main>
<footer class="footer-distributed bg-grey">

    <div class="footer-left">

        <h3>Bystru<span>ška</span></h3>

        <p class="footer-links">
            <a href="/#">@lang('words.domov')</a>
            ·
            <a href="/books">@lang('words.novinky')</a>
            ·
            <a href="/contact">@lang('words.kontakt')</a>
        </p>

        <p class="footer-company-name">Branko &copy; 2020</p>
    </div>

    <div class="footer-center">

        <div>
            <i class="fa fa-map-marker"></i>
            <p><span>Devínska Nová Ves</span> Bratislava, @lang('words.slovensko')</p>
        </div>

        <div>
            <i class="fa fa-phone"></i>
            <p>+421 9XX XXX XXX</p>
        </div>

        <div>
            <i class="fa fa-envelope"></i>
            <p><a href="mailto:kontakt@bystruska.sk">info@lesnyklubbystruska.sk</a></p>
        </div>

    </div>

    <div class="footer-right">

        <div class="footer-icons">

            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>

        </div>

    </div>

</footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script src="/resources/js/jquery.rwdImageMaps.min.js"></script>



<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>

<script>
    $(window).scroll(function(){
        $('.banner-scroll').toggleClass('banner-scroll', $(window).scrollTop() > $('#banner').offset().top);
    });
</script>

<script>
    function closeBanner() {
        var x = document.getElementById("banner");

        x.style.display = "none";
    }
</script>

<script>
    $(function (){
        $('.pop').on('click',
            function () {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');
            }
        );
    });
</script>

<script>
    $(window).load(function () {
        $(".trigger_popup_fricc").click(function(){
            $('.hover_bkgr_fricc').show();
        });
        $('.hover_bkgr_fricc').click(function(){
            $('.hover_bkgr_fricc').hide();
        });
        $('.popupCloseButton').click(function(){
            $('.hover_bkgr_fricc').hide();
        });
    });
</script>

<script>
    imageMapResize();
</script>
</body>
</html>


