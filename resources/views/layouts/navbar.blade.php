


<script>
    document.onreadystatechange = function () {
        var state = document.readyState
        if (state == 'interactive') {
            document.getElementById('contents').style.visibility="hidden";
        } else if (state == 'complete') {
            setTimeout(function(){
                document.getElementById('interactive');
                document.getElementById('load').style.visibility="hidden";
                document.getElementById('contents').style.visibility="visible";
            });
        }
    }
</script>
<style>
    #load{
        width:100%;
        height:100%;
        position:fixed;
        z-index:9999;
    }

    #load-img {
        display: block;
        margin-left: auto;
        margin-right: auto;
        vertical-align: middle;
        width: 15%;
    }
</style>
<div id="load" class="bg-light">
    <img src="http://www.genesisindia.net.in/images/loading.gif" id="load-img">
</div>

{{--Header ROW--}}

{{--<div class="header-row" style="width: 100%; max-height: 150px; background: #7a7a7a; display: none">
    <div class="container">
        <img src="{{URL::asset('img/logo_final.png')}}" alt="" class="img-fluid" style="width: auto; max-height: 150px; ">
    </div>

</div>--}}
<img src="{{URL::asset('img/title_WIP.png')}}" class="img-fluid" style="max-height: 400px; width: 100%">

<nav class="navbar sticky-top navbar-expand-md navbar-dark bg-grey">
{{--    <div class="container d-flex justify-content-between">--}}
        <a href="/" class="navbar-brand">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bug" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.355.522a.5.5 0 0 1 .623.333l.291.956A4.979 4.979 0 0 1 8 1c1.007 0 1.946.298 2.731.811l.29-.956a.5.5 0 1 1 .957.29l-.41 1.352A4.985 4.985 0 0 1 13 6h.5a.5.5 0 0 0 .5-.5V5a.5.5 0 0 1 1 0v.5A1.5 1.5 0 0 1 13.5 7H13v1h1.5a.5.5 0 0 1 0 1H13v1h.5a1.5 1.5 0 0 1 1.5 1.5v.5a.5.5 0 1 1-1 0v-.5a.5.5 0 0 0-.5-.5H13a5 5 0 0 1-10 0h-.5a.5.5 0 0 0-.5.5v.5a.5.5 0 1 1-1 0v-.5A1.5 1.5 0 0 1 2.5 10H3V9H1.5a.5.5 0 0 1 0-1H3V7h-.5A1.5 1.5 0 0 1 1 5.5V5a.5.5 0 0 1 1 0v.5a.5.5 0 0 0 .5.5H3c0-1.364.547-2.601 1.432-3.503l-.41-1.352a.5.5 0 0 1 .333-.623zM4 7v4a4 4 0 0 0 3.5 3.97V7H4zm4.5 0v7.97A4 4 0 0 0 12 11V7H8.5zM12 6H4a3.99 3.99 0 0 1 1.333-2.982A3.983 3.983 0 0 1 8 2c1.025 0 1.959.385 2.666 1.018A3.989 3.989 0 0 1 12 6z"/>
            </svg>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarHeader">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="/about" class="navbar-brand">@lang('words.oNas')</a>
                </li>
                <li class="nav-item">
                    <a href="/books" class="navbar-brand">@lang('words.novinky')</a>
                </li>
                <li class="nav-item">
                    <a href="/gallery" class="navbar-brand">@lang('words.galeria')</a>
                </li>
                <li class="nav-item">
                    <a href="/contact" class="navbar-brand">@lang('words.kontakt')</a>
                </li>
                <li class="nav-item">
                    <a href="/club" class="navbar-brand">@lang('words.klub')</a>
                </li>
            </ul>

            <ul class="nav navbar-nav ml-auto">
                @auth
                    <li class="nav-item dropdown">
                        <a class="navbar-brand btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('words.pridat')
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="/addimage">@lang('words.fotka')</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/addbook">@lang('words.aktualita')</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/users/{{ auth()->user()->id }}" class="navbar-brand">
                            @<strong>{{ auth()->user()->name }}</strong>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="navbar-brand">
                            @lang('words.odhlasitSa')
                        </a>
                        <form style="display: none" id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
                    </li>
                @endauth
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(Config::get('app.locale') == "sk")
                            <img src="{{URL::asset('img/Slovakia.png')}}" alt="">
                        @else
                            <img src="{{URL::asset('img/Uk.png')}}" alt="Englisg-flag">
                        @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="/setlocale/sk"><img src="{{URL::asset('img/Slovakia.png')}}" alt="Slovak-flag"></a>
                        <a class="dropdown-item" href="/setlocale/en"><img src="{{URL::asset('img/Uk.png')}}" alt="Englisg-flag"></a>
                    </div>
                </li>
            </ul>
        </div>
{{--    </div>--}}
</nav>

