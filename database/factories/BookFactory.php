<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    $title = $faker->words(4, true);

    return [
        'title' => ucfirst($title),
        'text' => $faker->paragraphs(5, true),
    ];
});
